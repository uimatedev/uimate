from django.db import models

# Create your models here.
class Barang(models.Model):
    PROVINSI = [
            ("ACEH"	, "Aceh"),
            ("SUMATRA UTARA" , "Sumatra Utara"),
            ("SUMATRA BARAT" , "Sumatra Barat"),
            ("RIAU" , "Riau"),
            ("KEPULAUAN RIAU" , "Kepulauan Riau"),
            ("JAMBI" , "Jambi"),
            ("BENGKULU" , "Bengkulu"),
            ("SUMATRA SELATAN" , "Sumatra Selatan"),
            ("KEPULAUAN BANGKA BELITUNG" , "Kepulauan Bangka Belitung"),
            ("LAMPUNG" , "Lampung"),
            ("BANTEN" , "Banten"),
            ("JAWA BARAT" , "Jawa Barat"),
            ("DKI JAKARTA" , "DKI Jakarta"),
            ("JAWA TENGAH" , "Jawa Tengah"),
            ("JAWA TIMUR" , "Jawa Timur"),
            ("DAERAH ISTIMEWA YOGYAKARTA" , "DI Yogyakarta"),
            ("BALI" , "Bali"),
            ("NUSA TENGGARA BARAT" , "Nusa Tenggara Barat"),
            ("NUSA TENGGARA TIMUR" , "Nusa Tenggara Timur"),
            ("KALIMANTAN BARAT" , "Kalimantan Barat"),
            ("KALIMANTAN SELATAN" , "Kalimantan Selatan"),
            ("KALIMANTAN TENGAH" , "Kalimantan Tengah"),
            ("KALIMANTAN TIMUR" , "Kalimantan Timur"),
            ("KALIMANTAN UTARA" , "Kalimantan Utara"),
            ("GORONTALO" , "Gorontalo"),
            ("SULAWESI SELATAN" , "Sulawesi Selatan"),
            ("SULAWESI TENGGARA" , "Sulawesi Tenggara"),
            ("SULAWESI TENGAH" , "Sulawesi Tengah"),
            ("SULAWESI UTARA" , "Sulawesi Utara"),
            ("SULAWESI BARAT" , "Sulawesi Barat"),
            ("MALUKU" , "Maluku"),
            ("MALUKU UTARA" , "Maluku Utara"),
            ("PAPUA" , "Papua"),
    ]
    nama = models.CharField(max_length = 30)
    harga = models.CharField(max_length = 30, default = "Rp")
    lokasi = models.CharField(max_length = 50, default = 'DKI JAKARTA', choices = PROVINSI)
    pemilik = models.CharField(max_length = 30)
    kontak = models.CharField(max_length = 30)
    gambar = models.URLField(default="https://i1.wp.com/www.anakui.com/wp-content/uploads/2015/06/jakun.jpg")
    deskripsi = models.TextField()