# Generated by Django 3.1.2 on 2020-10-22 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Barang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('harga', models.CharField(default='Rp.0', max_length=30)),
                ('lokasi', models.CharField(max_length=30)),
                ('pemilik', models.CharField(max_length=30)),
                ('kontak', models.CharField(max_length=30)),
                ('gambar', models.CharField(max_length=30)),
                ('deskripsi', models.TextField(max_length=30)),
            ],
        ),
    ]
