from django.urls import path

from . import views

app_name = 'PassingBarang'
urlpatterns = [
    path('', views.list_barang, name='index'),
    path('detil/<int:id_number>/', views.detil, name='detil'),
    path('tambah/', views.tambah, name = 'tambah'),
    path('detil/confirm/<int:id_number>/', views.confirm, name='confirm'),
    path('status/<area>/', views.area_status, name = 'status'),
]