from django.test import TestCase, Client
from . import views, models, forms
from django.urls import resolve
from django.http import HttpRequest
import requests
# Create your tests here.
BARANG ={
    'nama':'Buku Ajaib Kak Pewe',
    'harga':'Rp.0',
    'lokasi':'DKI JAKARTA',
    'pemilik':'Kak Pewe',
    'kontak':'@kakpeweunyu',
    'gambar':'https://indogamers.id/courier/2020/10/venti-genshin-impact-uhdpaper.com-4K-3.3040-wp.thumbnail.jpg',
    'deskripsi':'buku biasa yang diberi sihir oleh Kak Pewe secara personal',
}
BARANG_KOSONG ={
    'nama':'',
    'harga':'',
    'lokasi':'',
    'pemilik':'',
    'kontak':'',
    'gambar':'',
    'deskripsi':'',
}
class PassingBarangUnitTest(TestCase):
######################
# url
    def test_list_barang_url_exists(self):
        response = Client().get('/passing/')
        self.assertEqual(response.status_code,200)
    
    def test_detil_barang_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/1/')
        self.assertEqual(response.status_code,200)

    def test_tambah_barang_url_exists(self):
        response = Client().get('/passing/tambah/')
        self.assertEqual(response.status_code,200)

    def test_konfirmasi_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/confirm/1/')
        self.assertEqual(response.status_code,200)

    # TODO test for area status
    def test_area_status_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe", lokasi = "DKI JAKARTA")
        new_barang.save()
        response = Client().get('/passing/status/DKI%20JAKARTA/')
        self.assertEqual(response.status_code, 200  )

###################################
# views

    # test list barang
    def test_list_barang_view(self):
        func = resolve('/passing/')
        self.assertEqual(func.func, views.list_barang)

    def test_list_barang_redirection(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/', {'id':1})
        self.assertEqual(response.status_code, 302)

    def test_list_barang_redirec_to_correct_link(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/', {'id':1})
        self.assertRedirects(response, '/passing/detil/1/')

    # test detil barang 
    def test_detil_barang_view(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        func = resolve('/passing/detil/1/')
        self.assertEqual(func.func, views.detil)


    
    def test_detil_redirection_after_flag(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/1/', {"flag":"flag"})
        url = "/passing/detil/confirm/1/"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, url)

    def test_detil_redirection_status(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe", lokasi = "DKI JAKARTA")
        new_barang.save()
        response = Client().post('/passing/detil/1/', {"status":"status"})
        url = "/passing/status/DKI%20JAKARTA/"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, url)
    # test tambah barang
    def test_tambah_view(self):
        func = resolve('/passing/tambah/')
        self.assertEqual(func.func, views.tambah)

    def test_tambah_redirection(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertEqual(response.status_code, 302)

    def test_tambah_redirect_to_url(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertRedirects(response, '/passing/')

    def test_tambah_object_count(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertEqual(models.Barang.objects.all().count(), 1)
    # test konfirmasi
    def test_confirm_view(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        func = resolve('/passing/detil/confirm/1/')
        self.assertEqual(func.func, views.confirm)

    def test_confirm_deletion(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/confirm/1/', {"delete":"delete"})
        self.assertEqual(models.Barang.objects.all().count(), 0)

    def test_confirm_redirection_after_deletion(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/confirm/1/', {"delete":"delete"})
        self.assertRedirects(response, '/passing/')

    def test_area_status_view(self):
        func =resolve('/passing/status/DKI%20JAKARTA/')
        self.assertEqual(func.func, views.area_status)
##############################
# templates
    # test list_barang
    def test_list_barang_template(self):
        response = Client().get('/passing/')
        self.assertTemplateUsed(response, 'list_barang.html')

    # test detil barang
    def test_detil_barang_template(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/1/')
        self.assertTemplateUsed(response, 'detil_barang.html')

    # test tambah barang
    def test_tambah_barang_template(self):
        response = Client().get('/passing/tambah/')
        self.assertTemplateUsed(response, 'tambah_barang.html')

    # test konfirmasi
    def test_konfirmasi_template(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/confirm/1/')
        self.assertTemplateUsed(response, 'konfirmasi.html')
    # test area status
    def test_area_status_template(self):
        response = Client().get('/passing/status/DKI%20JAKARTA/')
        self.assertTemplateUsed(response, 'status.html')

    def test_area_status_report(self):
        data = requests.get('https://data.covid19.go.id/public/api/prov.json').json()['list_data']
        request = HttpRequest()
        response = views.area_status(request, "DKI JAKARTA").content.decode('utf8')
        self.assertIn(str(data[0]['jumlah_meninggal']), response)
#################################
# models
    # test barang model
    def test_barang_model_exist(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        self.assertEqual(models.Barang.objects.all().count(), 1)

#####################
# forms
    # test barang form
    def test_barang_form_reject_empty(self):
        form = forms.BarangForm(data =BARANG_KOSONG)
        self.assertFalse(form.is_valid())

    def test_barang_form_accept_test(self):
        form = forms.BarangForm(data =BARANG)
        self.assertTrue(form.is_valid())