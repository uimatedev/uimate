from django.shortcuts import render, redirect, reverse
from . import forms, models 
from django.http import HttpResponse
import requests

# Create your views here.
def list_barang(request):
    response ={
        'barang_list':models.Barang.objects.all(),
        'title': 'List Barang',
    }
    if request.method == 'POST':
        id_number = request.POST.get("id")
        return redirect ('detil/'+ id_number + '/')
    
    return render(request, "list_barang.html", response)

def detil(request, id_number):
    barang = models.Barang.objects.get(id = id_number)
    lokasi = barang.lokasi
    response = {
        'barang':barang,
        'title': "Detail Barang",
    }
    if "flag" in request.POST and request.method == 'POST':
        return redirect('PassingBarang:confirm', id_number = id_number)
    if "status" in request.POST and request.method == 'POST':
        return redirect('PassingBarang:status', area = lokasi)
    return render(request, 'detil_barang.html', response)

def tambah(request):
    form = forms.BarangForm(request.POST or None)
    if request.method == 'POST':
        form = forms.BarangForm(request.POST) 
        if form.is_valid():
            form.save()
            return redirect('/passing/')
    response ={
        'form':form,
        'title': 'Tambahkan Barang',
    }
    return render(request, 'tambah_barang.html', response)

def confirm(request, id_number):
    barang = models.Barang.objects.get(id = id_number)
    response = {
        'barang':barang,
        'title': 'Konfirmasi',
    }

    if 'delete' in request.POST and request.method == 'POST':
        barang.delete()
        return redirect ('/passing/')
    return render(request, 'konfirmasi.html', response)

def area_status(request, area):
    provinsi_list = requests.get('https://data.covid19.go.id/public/api/prov.json').json()['list_data']
    real_data ={}
    for provinsi_data in provinsi_list:
        if provinsi_data['key'] == area:
            real_data = provinsi_data
            break
    response ={
        'data': real_data,
        'title': 'Status Daerah ' + area,
    }
    return render(request, 'status.html', response)
