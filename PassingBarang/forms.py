from django.forms import ModelForm,Form
from .models import Barang
from django import forms

class BarangForm(ModelForm):
    class Meta:
        model = Barang
        fields =[
            'nama',
            'harga',
            'lokasi',
            'pemilik',
            'kontak',
            'gambar',
            'deskripsi',
        ]
        widgets={
            'nama': forms.TextInput(attrs={'placeholder': 'Masukan nama barang di sini'}),
            'harga': forms.TextInput(attrs={'placeholder': 'Masukan harga barang jika ada, serta berikan mata uangnya'}),
            'pemilik': forms.TextInput(attrs={'placeholder': 'Masukan namamu di sini'}),
            'kontak': forms.TextInput(attrs={'placeholder': 'Masukan kontak mu di sini (boleh dalam bentuk nomor WA, id line, dll)'}),
            'gambar': forms.TextInput(attrs={'placeholder': 'Masukan link gambar (gunakan image hosting site sepert imgur)'}),
            'deskripsi': forms.Textarea(attrs={'placeholder': 'deskripsi singkat barang'}),
        }
     