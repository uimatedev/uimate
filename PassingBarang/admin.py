from django.contrib import admin
from .models import Barang
# Register your models here.

class BarangAdmin(admin.ModelAdmin):
    admin.site.register(Barang)