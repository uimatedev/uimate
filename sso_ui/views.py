"""SSO UI views module."""
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from kenalan.models import Pertemuan
from .models import Profile
from .forms import ProfileForm

# @login_required(login_url='sso_ui:login')
def profile(request):
    """Render profile page."""
    context = dict()
    if(request.user.is_authenticated):
        profil = get_object_or_404(Profile, id = request.user.id)
        context["profile"] = profil
        context["pertemuans"] = profil.peserta.all()
        context["form"] = ProfileForm(request.POST or None, instance=get_object_or_404(Profile, id = request.user.id))
        if(request.POST and context["form"].is_valid()):
            context["form"].save()
            return redirect("sso_ui:profile")
    
    return render(request, "profile.html", context)
