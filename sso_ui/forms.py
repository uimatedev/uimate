from django.forms import ModelForm, ChoiceField
from .models import Profile
from crispy_forms.helper import FormHelper

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['color']
        
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False 