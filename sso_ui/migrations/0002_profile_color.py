# Generated by Django 3.1.2 on 2020-11-19 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sso_ui', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='color',
            field=models.CharField(default='blue', max_length=10, verbose_name='warna tampilan'),
        ),
    ]
