from django.db import models

# Create your models here.
class Tukaran(models.Model):
    nama = models.CharField(max_length=100)
    makara = models.CharField(max_length=100)
    ukuran = models.CharField(max_length=100)
    kontak = models.CharField(max_length=100)