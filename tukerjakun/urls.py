from django.urls import path

from . import views

app_name = 'tukerjakun'
urlpatterns = [
    path('', views.get_formulir, name='index'),
    path('formulir/', views.get_formulir, name='tuker'),
    path('get_hasil_formulir/', views.get_hasil_formulir, name='tukerjakun'),
]