from django.test import TestCase, Client
from .models import Tukaran

# Create your tests here.
class TestTukerJakun(TestCase):
    def test_adakah_url_formulir(self):
        response = Client().get('/jakun/formulir/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_submit_dikirim_ke_hasil(self):
        response = Client().get('/jakun/get_hasil_formulir/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_hasil_formulirnya(self):
        response = Client().get('/jakun/get_hasil_formulir/')
        html_return = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'tukerjakun.html')
        self.assertIn("Tambahin Jakun", html_return)

    def test_apakah_sudah_ada_model_Tukaran(self):
        Tukaran.objects.create(nama="NAMA", makara="MAKARA", ukuran="UKURAN", kontak="KONTAK")
        hitung_jumlah_buku = Tukaran.objects.all().count()
        self.assertEquals(hitung_jumlah_buku, 1)

    def test_adakah_handle_post_di_halaman_get_hasil_formulir(self):
        response = Client().post('/jakun/get_hasil_formulir/',{'nama':'NAMA','makara':'MAKARA', 'ukuran':'UKURAN', 'kontak':'KONTAK'})
        html_return = response.content.decode('utf8')
        self.assertIn("Tambahin Jakun", html_return)
        self.assertIn("NAMA", html_return)
        self.assertIn("MAKARA", html_return)
        self.assertIn("UKURAN", html_return)
        self.assertIn("KONTAK", html_return)
