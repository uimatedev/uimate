from django.shortcuts import render
from django.http import HttpResponse
from .models import Tukaran

# Create your views here.
def get_formulir(request):
    response ={}
    return render(request,'formulir.html', response)

def get_hasil_formulir(request):
    if request.method == 'POST':
        Tukaran.objects.create(nama=request.POST['nama'], makara=request.POST['makara'], ukuran=request.POST['ukuran'], kontak=request.POST['kontak'])
    jakun =Tukaran.objects.all()
    response ={'jakun':jakun}
    return render(request,'tukerjakun.html', response)