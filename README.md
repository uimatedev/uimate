# UImate

[![pipeline status](https://gitlab.com/uimatedev/uimate/badges/master/pipeline.svg)](https://gitlab.com/uimatedev/uimate/-/commits/master)
[![coverage report](https://gitlab.com/uimatedev/uimate/badges/master/coverage.svg)](https://gitlab.com/uimatedev/uimate/-/commits/master)

UImate dapat diakses di sini [UImate](https://uimate.herokuapp.com).

## Daftar isi

- [UImate](#uimate)
  - [Daftar isi](#daftar-isi)
  - [Developer](#developer)
  - [Apa itu UImate?](#apa-itu-uimate)
    - [Forum Tukar Jakun untuk Mahasiswa Baru yang menjadi keluarga UI.](#forum-tukar-jakun-untuk-mahasiswa-baru-yang-menjadi-keluarga-ui)
    - [Halaman Utama, Tentang Kami](#halaman-utama-tentang-kami)
    - [Membuat countdown PSBB atau kondisi setiap daerah.](#membuat-countdown-psbb-atau-kondisi-setiap-daerah)
    - [Mengenal mahasiswa](#mengenal-mahasiswa)
    - [Waris barang](#waris-barang)
    - [Dan berbagai fitur-fitur menarik lain yang masih akan dikembangkan.](#dan-berbagai-fitur-fitur-menarik-lain-yang-masih-akan-dikembangkan)
  - [Instruksi penggunaan](#instruksi-penggunaan)
  - [Lanjutan](#lanjutan)
  - [Tips pengembangan](#tips-pengembangan)

## Developer

| Nama                               | ID         | Gitlab                                                      |
| ---------------------------------- | ---------- | ----------------------------------------------------------- |
| Hocky Yudhiono                     | 1906285604 | [hocky](https://gitlab.com/hocky)                           |
| Gabriel Enrique                    | 1906293032 | [gabriel.enrique](https://gitlab.com/gabriel.enrique)       |
| Zahidan Asaduddin Purnama          | 1906302636 | [namasayazidan](https://gitlab.com/namasayazidan)           |
| Bonifasius Erlangga Einsoni Rienko | 1906302850 | [BonifasiusErlangga](https://gitlab.com/BonifasiusErlangga) |


## Apa itu UImate?

UImate ialah aplikasi asisten mahasiswa UI dalam masa PJJ yang disebabkan karena adanya pandemi Covid-19 🤮.

Konsepnya mirip dengan iMaba yang dibuat oleh Tim Media BEM FASILKOM UI 🖼️. Namun lebih untuk fungsionalitas belajar-mengajar dan berkomunikasi antara setiap elemen 📖.

Fitur-fitur yang akan dikembangkan antara lain sebagai berikut.

### Forum Tukar Jakun untuk Mahasiswa Baru yang menjadi keluarga UI.

Jaket Kuning atau biasa disingkat Jakun, merupakan jaket almamater berwarna kuning khas Universitas Indonesia 💛. Ukuran jakun yang kurang pas telah menjadi masalah bagi mahasiswa, terutama mahasiswa baru yang baru saja mendapatkan jakunnya 👕. UImate akan menyediakan forum yang dapat digunakan sebagai sarana tukar-menukar jakun di masa pandemi Covid-19 ini, dan bahkan dapat dikembangkan lebih jauh untuk mendukung pertukaran setelah masa pandemi ini 👼. Dengan adanya forum ini, mahasiswa akan lebih leluasa memilih berbagai kriteria jakun, serta tetap menjaga protokol kesehatan dengan mengatur pertemuan pada tempat yang lebih aman dari Covid-19 ini 🏥.

### Halaman Utama, Tentang Kami

Halaman utama kami menyediakan berbagai navigasi untuk fitur-fitur UImate lainnya. Ada pula random quotes generator yang diambil dari berbagai kutipan yang telah diberikan orang-orang, layaknya si bangku untuk tetap semangat selama masa pandemi Covid-19 ini 🌴.

### Membuat countdown PSBB atau kondisi setiap daerah.

Mahasiswa yang ada biasanya berasal dari berbagai daerah, dengan adanya halaman profil dan pengecekan kondisi Covid-19 ini, kita dapat mengetahui kondisi daerah teman lainnya 🗺️.

### Mengenal mahasiswa

Sulitnya bersalam jumpa secara daring akan membuat mahasiswa menjadi lebih sedikit bertemu tatap muka satu sama lain. Dengan adanya fitur ini, pertemuan yang diatur akan menjadi lebih mudah 📰. Setiap orang akan dapat memberikan semacam "RSVP" untuk kehadiran pertemuan tatap muka, terutama dalam rangka menyukseskan pembinaan mahasiswa baru, atau sekadar menghabiskan waktu luang untuk *refreshing* bersama teman-teman mahasiswa lain 🎲.

### Waris barang

Masa pandemi Covid-19 ini membuat kakak-kakak tingkat kita wisuda secara daring. Tentunya mereka akan meninggalkan tempat tinggal (indekos atau apartemen) mereka 🏛️. Waris barang membuat barang-barang yang mungkin masih dibutuhkan oleh adik-adik tingkat lebih mudah disalurkan sesuai dengan protokol kesehatan 📖.

### Dan berbagai fitur-fitur menarik lain yang masih akan dikembangkan.

## Instruksi penggunaan

1. Clone repository ini.

   ```shell
   git clone https://gitlab.com/uimatedev/uimate.git
   ```

2. Buat Python *virtual environment* di dalamnya.

   ```shell
   python -m venv venv
   ```

   > Catatan: sesuaikan dengan *executable* `python` yang ada di komputer kamu,
   > karena terkadang (misal: di Ubuntu atau macOS) Python 3 hanya bisa
   > dipanggil dengan `python3`, bukan `python`.

3. Aktifkan *virtual environment* yang telah dibuat.\
   Di Windows:

   ```shell
   venv\Scripts\activate
   ```

   Di Linux/macOS:

   ```shell
   source venv/bin/activate
   ```

   Jika berhasil, akan muncul `(venv)` pada *prompt* cmd/terminal kamu.

4. Instal Django pada *virtual environment* tersebut. 

   ```shell
   python -m pip install Django
   ```

5. Lanjutkan dengan membuat basis data lokal dan mengumpulkan berkas *static* menjadi satu direktori dengan perintah-perintah berikut.

   ```shell
   python manage.py migrate
   python manage.py collectstatic
   ```

6. Jika sudah, kamu bisa menjalankan *web server* kamu secara lokal dengan perintah berikut.

   ```shell
   python manage.py runserver
   ```

7. Mulai dari sini, kamu cukup edit berkas-berkas proyek Django kamu sesuai kebutuhan. Lalu, jangan lupa gunakan perintah `git add`, `git commit`, dan `git push` untuk mengunggah perubahanmu ke GitLab/GitHub (yang kemudian akan di-*deploy* ke Heroku). Jangan lupa untuk membuat berkas-berkas *migrations* jika kamu mengubah berkas `models.py`.

   ```shell
   python manage.py makemigrations
   ```

   Berkas-berkas *migrations* yang dihasilkan **harus** dimasukkan ke dalam
   repositori.

8. Untuk menjalankan *unit test*, kamu bisa gunakan perintah berikut.

   ```shell
   python manage.py test --exclude-tag=functional
   ```

9. Untuk memasukkan kodemu, buat branch baru.

   ```shell
   git checkout master
   git checkout -b <nama>-<app>
   ```

   Misalnya sebagai berikut.

   ```shell
   git checkout master
   git checkout -b hocky-landing-page
   ```

   Untuk mengakses branch yang sudah ada, tidak perlu memakai flag -b. Untuk kembali ke last commit, gunakan command berikut.

   ```shell
   git reset --hard
   ```

   Untuk commit lakukan command berikut.

   ```shell
   git add .
   git commit
   git push
   ```

   Untuk membuat app baru

   ```shell
   python manage.py startapp <nama_app>
   ```

   Perhatikan app `main`, sudah ada contoh penggunaan `base.html` dan bagian `views`nya. Pada `UImate/settings.py` jangan lupa untuk menambahkan installed apps, dan `UImate/urls.py` tambahkan pula path ke app-mu.

## Lanjutan

Setelah berhasil membuat proyek Django baru dengan templat ini, ada
langkah-langkah lanjutan yang sangat direkomendasikan untuk dikerjakan demi
kemudahan pengembangan web kamu ke depannya.

1. Unduh `chromedriver` (atau *webdriver* lain yang kamu inginkan) untuk
   komputer kamu agar bisa menjalankan *functional test* secara lokal.

   Di Windows:\
   Unduh [ChromeDriver][chromedriver] (`chromedriver_win32.zip`) untuk versi
   Chrome yang kamu gunakan. Lalu, ekstrak berkas `.zip` tersebut dan letakkan
   `chromedriver.exe` di direktori ini (setara dengan `manage.py`).

   Di Linux:\
   Beberapa distribusi Linux memiliki *package* khusus untuk ChromeDriver, atau
   sebagian juga sudah menyertakannya di dalam *package* `chromium` (misal:
   Arch Linux). Jika tidak ada *package* yang sesuai, kamu bisa unduh
   `chromedriver_linux64.zip` melalui pranala di atas, lalu `unzip` dan
   letakkan `chromedriver` ke salah satu direktori yang ada di `$PATH`.

   Di macOS:\
   Instal `chromedriver` dengan [Homebrew][homebrew]
   (`brew cask install chromedriver` di Terminal). Silakan instal Homebrew
   terlebih dahulu jika belum. Saya tidak punya perangkat dengan macOS, jadi
   silakan cari solusinya apabila ada kendala yang muncul ;)

   Untuk menjalankan *functional test* saja, gunakan perintah berikut:

   ```shell
   python manage.py test --tag=functional
   ```

   > Catatan: jika ingin menggunakan *webdriver* lain, silakan sesuaikan berkas
   > `tests.py` kamu serta lakukan instalasi untuk *webdriver* tersebut.
   > Jika ingin melihat proses *functional test* di komputer kamu, nonaktifkan
   > opsi `headless` pada `tests.py`. Namun, jangan lupa untuk mengaktifkannya
   > kembali karena opsi tersebut dibutuhkan oleh GitLab CI/GitHub Actions.
   > Pastikan kamu sudah menjalankan perintah `collectstatic` sebelum
   > menjalankan *functional test*.

   > Catatan khusus pengguna Windows: ketika kamu menjalankan
   > *functional test*, mungkin akan muncul *error* seperti
   > `ConnectionResetError: [WinError 10054] An existing connection was forcibly
   > closed by the remote host`. Selama tes tetap berjalan sampai selesai,
   > abaikan saja masalah tersebut. Ini memang [*bug*][ticket-21227] yang belum
   > diperbaiki untuk Django di Windows.

2. Atur konfigurasi *code coverage* di GitLab.

   Untuk menjalankan tes dengan pengukuran *coverage* pada komputer lokal kamu,
   gunakan perintah berikut.

   ```shell
   coverage run --include="./*" --omit="venv/*,manage.py,UImate/*" manage.py test
   ```

   > Catatan: Perintah ini akan menjalan *unit test* dan *functional test*
   > sekaligus, jadi pastikan kamu sudah menjalankan perintah `collectstatic`
   > sebelum menjalankan tes.

## Tips pengembangan

1. Mulai sekarang, **biasakan** untuk me-*reload* *browser* kamu dengan
   <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>R</kbd> (beberapa *browser* juga bisa
   dengan <kbd>Shift</kbd>+<kbd>F5</kbd>), bukan hanya
   <kbd>Ctrl</kbd>+<kbd>R</kbd> atau <kbd>F5</kbd>. Jika tidak, ketika kamu
   mengembangkan web secara lokal, kamu mungkin akan sering menemukan kasus di
   mana berkas *static* yang muncul di *browser* tidak berubah setelah kamu
   memodifikasinya. Ini terjadi karena *browser* membuat *cache* dari berkas
   *static* kamu supaya dapat diakses lebih cepat. Dengan menekan tombol
   <kbd>Shift</kbd>, *browser* akan [mem-*bypass* berkas *static* yang sudah
   di-*cache*][bypass-cache].
2. Biasakan untuk menulis tes yang berkualitas untuk proyek web kamu, terutama
   untuk logika-logika seperti yang ada pada bagian *views*. Membuat tes akan
   memudahkan kamu untuk menemukan *bug* pada program kamu lebih awal. Selain
   itu, tes juga dapat mencegah kamu menciptakan *bug* baru ketika
   mengembangkan proyek web kamu.
3. *Code coverage* 100% tidak menjamin proyek kamu bebas *bug*. Namun, memiliki
   tes yang menguji proyek kamu secara keseluruhan tentu akan sangat
   bermanfaat. Mulailah menulis tes setiap kali kamu membuat fungsionalitas
   baru agar *code coverage* tetap terjaga.