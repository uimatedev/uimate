from django_cas_ng.signals import cas_user_authenticated
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.utils import timezone
from .models import *
from kenalan.models import Pertemuan
from sso_ui.models import Profile

import datetime
import re

class UnauthenticatedTestCase(TestCase):
    def test_kirim_pesan_unauthenticated(self):
        response = self.client.post("/kirim_pesan/", { 'content': 'PESAN', 'author': ''})
        self.assertIn('/login/', response.url)
    
    def test_if_welcome_text_exist(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertIn("Selamat Datang", content)
        self.assertIn("Pahlawan Muda", content)

class MainTestCase(TestCase):

    ATTRIBUTES = {
        "nama": "Bukan Ice Bear",
        "peran_user": "mahasiswa",
        "npm": "1706123123",
        "kd_org": "01.00.12.01"
    }

    def setUp(self):
        """Set up test."""
        self.user = User.objects.create_superuser(
            username='username', password='password', email='username@test.com'
        )

        cas_user_authenticated.send(
            sender=self,
            user=self.user,
            created=False,
            attributes=MainTestCase.ATTRIBUTES
        )

        self.client.login(username='username', password='password')
    
    def test_logged_in(self):
        self.assertEqual(Profile.objects.all().count(), 1)
    
    def test_home_page_exists(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_home_page_use_template(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, 'main/home.html')

    def test_if_welcome_text_exist_and_use_user_full_name(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertIn("Selamat Datang", content)
        self.assertIn(self.user.get_full_name(), content)

    def test_if_message_input_form_exist(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        regex_match = re.search("<form((.|\n)*)<\/form>", content)
        self.assertTrue(regex_match)

    def test_if_message_input_field_exists(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        regex_match = re.search('<input(.*)placeholder="Kirim pesan juga yuk..."(.*)>', content)
        self.assertTrue(regex_match)

    def test_if_message_send_button_exists(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        regex_match = re.search('<button(.*)type="submit"(.*)Kirim(.*)<\/button>', content)
        self.assertTrue(regex_match)
    
    def test_if_covid_info_header_exists(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertIn("Kondisi terkini COVID-19 di Indonesia...", content)

    def test_if_covid_info_exists(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        regex_match_terkonfirmasi = re.search('<([a-z]+)(.*)Terkonfirmasi<\/([a-z]+)>(\n*)( *)<([a-z]+)(.*)([0-9]+)<\/([a-z]+)>', content)
        regex_match_dirawat = re.search('<([a-z]+)(.*)Dirawat<\/([a-z]+)>(\n*)( *)<([a-z]+)(.*)([0-9]+)<\/([a-z]+)>', content)
        regex_match_sembuh = re.search('<([a-z]+)(.*)Sembuh<\/([a-z]+)>(\n*)( *)<([a-z]+)(.*)([0-9]+)<\/([a-z]+)>', content)
        regex_match_meninggal = re.search('<([a-z]+)(.*)Meninggal<\/([a-z]+)>(\n*)( *)<([a-z]+)(.*)([0-9]+)<\/([a-z]+)>', content)
        self.assertTrue(regex_match_terkonfirmasi)
        self.assertTrue(regex_match_dirawat)
        self.assertTrue(regex_match_sembuh)
        self.assertTrue(regex_match_meninggal)
    
    def test_if_newest_events_section_wont_exists_if_no_upcoming_events_exists(self):
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertNotIn("Sebentar lagi akan ada acara ini...", content)
        self.assertNotIn("PertemuanKu", content)
        self.assertNotIn('oleh ' + self.user.get_full_name(), content)

        pembuat = Profile.objects.get(id=1)
        pertemuan = Pertemuan.objects.create(
            nama = 'PertemuanKu',
            pembuat = pembuat,
            waktu_mulai = datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(days=1),
            durasi = '15 Menit',
            kapasitas = 10,
            gambar = 'https://uploadstatic-sea.mihoyo.com/contentweb/20200616/2020061611214389168.png'
        )
        
        pertemuan.save()
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertIn("Sebentar lagi akan ada acara ini...", content)
        self.assertIn("PertemuanKu", content)
        self.assertIn('oleh ' + self.user.get_full_name(), content)
    
    def test_if_event_has_passed_then_it_will_not_be_displayed(self):
        pembuat = Profile.objects.get(id=1)
        pertemuan = Pertemuan.objects.create(
            nama = 'PertemuanKu',
            pembuat = pembuat,
            waktu_mulai = datetime.datetime.now(tz=timezone.utc) - datetime.timedelta(days=1),
            durasi = '15 Menit',
            kapasitas = 10,
            gambar = 'https://uploadstatic-sea.mihoyo.com/contentweb/20200616/2020061611214389168.png'
        )
        
        pertemuan.save()
        response = self.client.get("/")
        content = response.content.decode('utf8')
        self.assertNotIn("Sebentar lagi akan ada acara ini...", content)
        self.assertNotIn("PertemuanKu", content)
        self.assertNotIn('oleh ' + self.user.get_full_name(), content)
        
       