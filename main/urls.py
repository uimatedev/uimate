from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('kirim_pesan/', views.kirim_pesan, name='kirim_pesan')
]
