from django.contrib import messages
from django.shortcuts import render, redirect
from .models import Pesan
from .forms import PesanForm
from kenalan.models import Pertemuan
from PassingBarang.models import Barang
from tukerjakun.models import Tukaran

import copy, urllib, random, json, datetime, pytz, random

colors = ["red", "orange", "yellow", "green", "blue", "purple", "violet"]
emoji = ['😀', '😅', '🤭', '🤟', '🤣', '🙃', '😉', '😛', '🍀', '🥰', '🤪', '👽', '👻', '👀']

original = {
    "description" : "UImate",
    "title" : "UImate",
    "need_login" : False
}

def getNewestPertemuan():
    today_date = datetime.datetime.now(pytz.utc)
    pertemuans = Pertemuan.objects.order_by('waktu_mulai')
    
    newest_pertemuan = []
    for pertemuan in pertemuans:
        if pertemuan.waktu_mulai > today_date:
            newest_pertemuan.append(pertemuan)

    newest_pertemuan = newest_pertemuan[:min(7, len(newest_pertemuan))]
    return newest_pertemuan if newest_pertemuan else None

def getBarangWarisan():
    barang_warisan = list(Barang.objects.all())

    daftar_barang_pilihan = list()
    while (len(daftar_barang_pilihan) < 7) and (len(barang_warisan) > 0):
        barang_pilihan = random.choice(barang_warisan)
        daftar_barang_pilihan.append(barang_pilihan)
        barang_warisan.remove(barang_pilihan)

    return daftar_barang_pilihan

def getJakun():
    daftar_jakun = list(Tukaran.objects.all())

    daftar_jakun_pilihan = list()
    while (len(daftar_jakun_pilihan) < 7) and (len(daftar_jakun) > 0):
        jakun_pilihan = random.choice(daftar_jakun)
        daftar_jakun_pilihan.append(jakun_pilihan)
        daftar_jakun.remove(jakun_pilihan)

    return daftar_jakun_pilihan

def getDataCovid():
    with urllib.request.urlopen("https://data.covid19.go.id/public/api/update.json") as url:
        data = json.loads(url.read().decode())
    return data["update"]["total"]

def home(request):
    context = copy.copy(original)
    context['forms'] = PesanForm(request.POST or None)
    context['random_pesan'] = random.choice(Pesan.objects.all()) if Pesan.objects.all() else None
    context['newest_pertemuans'] = getNewestPertemuan()
    context['barang_pilihan'] = getBarangWarisan()
    context['daftar_jakun'] = getJakun()
    context['data_covid'] = getDataCovid()
    context["random_emoji"] = random.choice(emoji)
    return render(request, 'main/home.html', context)

def kirim_pesan(request):
    context = copy.copy(original)
    form = PesanForm(request.POST or None)
    if request.user.is_authenticated:
        if form.is_valid():
            form.save()
            messages.success(request, 'Pesan berhasil dikirim')
            return redirect('main:home')
        else:
            messages.error(request, 'Pesanmu terlalu panjang')
            return redirect('main:home')
    else:
        return redirect('sso_ui:login')
    