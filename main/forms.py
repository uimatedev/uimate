from django.forms import ModelForm, TextInput
from .models import *

class PesanForm(ModelForm):
    class Meta:
        model = Pesan
        fields = '__all__'

        labels = {
            'content': ''
        }

        widgets = {
            'content': TextInput(attrs={
                'placeholder': 'Kirim pesan juga yuk...'
            })
        }
    
