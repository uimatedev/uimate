from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.db import models
from sso_ui.models import Profile

# Create your models here.
class Pesan(models.Model):
    content = models.CharField(max_length=100)
    author = models.ForeignKey(Profile, related_name="author", on_delete=models.CASCADE)
