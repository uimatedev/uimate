from django.contrib import admin
from .models import Pertemuan, Komentar

# Register your models here.
class PertemuanAdmin(admin.ModelAdmin):
    admin.site.register(Pertemuan)

class KomentarAdmin(admin.ModelAdmin):
    admin.site.register(Komentar)