from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from sso_ui.models import Profile
from django.db import models

from datetime import datetime, timedelta

def get_str_time(menit):
    jam = menit//60
    menit -= jam*60
    ret = (f'{jam} Jam' if jam > 0 else '')
    if(jam > 0 and menit > 0): ret += ' '
    ret += (f'{menit} Menit' if menit > 0 else '')
    return ret

class Pertemuan(models.Model):
    nama = models.CharField(max_length=30, default = "Pertemuan asyik!")
    pembuat = models.ForeignKey(Profile, related_name="pembuat", on_delete=models.CASCADE)
    peserta = models.ManyToManyField(Profile, related_name="peserta")
    waktu_mulai = models.DateTimeField()
    durasi = models.CharField(max_length=30, choices=[(get_str_time(x), get_str_time(x)) for x in [5, 10, 15, 30, 45, 60, 90, 120, 150, 180, 210, 240]], default = '1 Jam')
    kapasitas = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(40)], default = 10)
    gambar = models.URLField(default="https://static.wikia.nocookie.net/gensin-impact/images/c/cf/Character_Venti_Portrait.png/revision/latest/scale-to-width-down/1000?cb=20200916184309")

    def __str__(self):
        return self.nama
    class Meta:
        ordering = ["waktu_mulai"]

class Komentar(models.Model):
    konten = models.TextField()
    waktu = models.DateTimeField(auto_now_add=True, blank=True)
    pertemuan = models.ForeignKey(Pertemuan, related_name="komentar", on_delete=models.CASCADE)
    penulis = models.ForeignKey(Profile, related_name="penulis", on_delete=models.CASCADE)
    def __str__(self):
        return self.konten
    class Meta:
        ordering = ["waktu"]
    