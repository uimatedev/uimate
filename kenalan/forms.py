from django.forms import ModelForm, DateTimeInput, CheckboxInput, Textarea
from .models import *

class PertemuanForm(ModelForm):
    class Meta:
        model = Pertemuan
        
        fields = '__all__'
        labels = {
            'nama': 'Nama Pertemuan',
            'waktu_mulai': 'Waktu Mulai'
        }
        widgets = {
            'waktu_mulai': DateTimeInput(attrs={'class': 'datetimepicker'})
        }

class KomentarForm(ModelForm):
    class Meta:
        labels = {
            'konten' : 'Diskusi'
        }
        model = Komentar
        fields = '__all__'
        widgets = {
            'konten': Textarea(attrs={'rows': 3, 'cols': 50}),
        }