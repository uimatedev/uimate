from django.test import TestCase
from django_cas_ng.signals import cas_user_authenticated
from django.contrib.auth.models import User
from .models import *
import datetime
from django.utils import timezone

class UnauthenticatedTestCase(TestCase):
    def test_tambah_unauthenticated(self):
        response = self.client.get("/kenalan/tambah/")
        self.assertEqual(response.status_code, 200)
        html = response.content.decode("utf8")
        self.assertIn("Login dulu", html)

class MainTestCase(TestCase):

    ATTRIBUTES = {
        "nama": "Ice Bear",
        "peran_user": "mahasiswa",
        "npm": "1706123123",
        "kd_org": "01.00.12.01"
    }

    def setUp(self):
        """Set up test."""
        self.user = User.objects.create_superuser(
            username='username', password='password', email='username@test.com'
        )

        cas_user_authenticated.send(
            sender=self,
            user=self.user,
            created=False,
            attributes=MainTestCase.ATTRIBUTES
        )

        self.client.login(username='username', password='password')
        
    def test_logged_in(self):
        self.assertEqual(Profile.objects.all().count(), 1)

    def test_halaman_index(self):
        response = self.client.get("/kenalan/")
        self.assertEqual(response.status_code, 200)

    def test_halaman_daftar_pertemuan(self):
        response = self.client.get("/kenalan/pertemuan/")
        self.assertEqual(response.status_code, 200)

    def test_halaman_tambah_pertemuan(self):
        response = self.client.get("/kenalan/tambah/")
        self.assertEqual(response.status_code, 200)

    def test_tambah_detail_hapus_pertemuan(self):
        preCnt = Pertemuan.objects.all().count()
        response = self.client.post(
            "/kenalan/tambah/", data={
                "nama": "Pertemuan Test 1",
                "pembuat": f'{self.user.id}',
                "peserta": f'{self.user.id}',
                "waktu_mulai": datetime.datetime.now(tz=timezone.utc), "durasi": "15 Menit",
                "kapasitas": 10,
                "gambar": "https://uploadstatic-sea.mihoyo.com/contentweb/20200616/2020061611214389168.png"})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/kenalan/pertemuan/')
        self.assertEqual(Pertemuan.objects.all().count(), preCnt+1)
        
        # Mengambil id pertemuan
        idPertemuan = Pertemuan.objects.all()[0].id

        # cek id pertemuan awal, sudah terdaftar
        response = self.client.get(f'/kenalan/detail/{idPertemuan}/')

        html = response.content.decode("utf8")

        self.assertEqual(response.status_code, 200)
        self.assertIn("Pertemuan Test 1", html)
        self.assertTemplateUsed(response, "detail_pertemuan.html")

        self.assertIn("Batal hadir", html)
        
        # Coba toggle pakai method get
        response = self.client.get("/kenalan/toggle_hadir/")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/kenalan/pertemuan/')
        
        peserta = Pertemuan.objects.get(id=idPertemuan).peserta.all()[0]
        
        # Coba batal hadir
        response = self.client.post(
            "/kenalan/toggle_hadir/", data={
                "peserta":peserta.id,
                "pertemuan":idPertemuan
            }
        )

        self.assertEqual(Pertemuan.objects.get(id=idPertemuan).peserta.count(), 0)

        # Mengecek apakah sudah batal hadir pada pertemuan ini
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get(f'/kenalan/detail/{idPertemuan}/')
        self.assertEqual(response.status_code, 200)
        html = response.content.decode("utf8")

        self.assertNotIn("Batal hadir", html)
        self.assertIn("Kosong!", html)

        # Coba daftar hadir
        response = self.client.post(
            "/kenalan/toggle_hadir/", data={
                "peserta":peserta.id,
                "pertemuan":idPertemuan
            }
        )
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get(f'/kenalan/detail/{idPertemuan}/')
        html = response.content.decode("utf8")

        # Mengecek apakah sudah hadir pada pertemuan ini
        self.assertIn(self.user.get_full_name(), html)
        self.assertIn("Batal hadir", html)

        self.assertEqual(Pertemuan.objects.get(id=idPertemuan).peserta.count(), 1)

        self.client.post(
            f"/kenalan/hapus/{idPertemuan}/",
            data={"pembuat":peserta.id}
        )

        response = self.client.get(f'/kenalan/detail/{idPertemuan}/')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Pertemuan.objects.all().count(), preCnt)
    
    def test_tambah_hapus_komentar(self):
        preCnt = Pertemuan.objects.all().count()
        response = self.client.post(
            "/kenalan/tambah/", data={
                "nama": "Pertemuan Test 1",
                "pembuat": f'{self.user.id}',
                "peserta": f'{self.user.id}',
                "waktu_mulai": datetime.datetime.now(tz=timezone.utc), "durasi": "15 Menit",
                "kapasitas": 10,
                "gambar": "https://uploadstatic-sea.mihoyo.com/contentweb/20200616/2020061611214389168.png"})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/kenalan/pertemuan/')
        self.assertEqual(Pertemuan.objects.all().count(), preCnt+1)
        
        # Mengambil id pertemuan
        idPertemuan = Pertemuan.objects.all()[0].id

        # cek id pertemuan awal, sudah terdaftar
        response = self.client.get(f'/kenalan/detail/{idPertemuan}/')

        html = response.content.decode("utf8")

        self.assertEqual(response.status_code, 200)
        self.assertIn("Pertemuan Test 1", html)
        self.assertTemplateUsed(response, "detail_pertemuan.html")

        self.assertIn("Batal hadir", html)
        
        # Mencoba posting komentar
        response = self.client.post(f'/kenalan/komentar/{idPertemuan}/', data={
            'konten': "Halo ini adalah komentar",
            'pertemuan': str(idPertemuan),
            'penulis': str(self.user.id),
        })
        self.assertEqual(Komentar.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, f'/kenalan/detail/{idPertemuan}/')
    
    def tearDown(self):
        self.client.logout()