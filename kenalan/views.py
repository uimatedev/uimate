from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib import messages
from django.http import Http404
from .models import *
from .forms import *
import copy, random
from django.utils import timezone
colors = ["red", "orange", "yellow", "green", "blue", "purple", "violet"]

original = {
    "description" : "Kenalan, Yuk!",
    "title" : "Kenalan, Yuk!",
    "need_login" : False
}

def get_hour(deltaHour):
    now = timezone.now()
    # Rounds to nearest hour by adding a timedelta hour if minute >= 30
    return (now.replace(second=0, microsecond=0, minute=0, hour=now.hour)
               +timedelta(hours=(now.minute//30)+deltaHour))

def get_random_title():
    title = ["Pertemuan seru!", "Main game bareng!", "Cari token!", "Ayo belajar matdas!", "Ayo main Genshin!", "Dota Yuk!!!"]
    return random.choice(title)

def index(request):
    context = copy.copy(original)
    return render(request, "kenalan.html", context)

def daftar_pertemuan(request):
    context = copy.copy(original)
    context["description"] = "Lihat-lihat ada pertemuan apa aja"
    context["title"] = "Daftar Pertemuan"
    context["pertemuans"] = Pertemuan.objects.all()
    return render(request, "daftar_pertemuan.html", context)

def tambah_pertemuan(request):
    context = copy.copy(original)
    context["description"] = "Tambahkan pertemuan baru di sini"
    context["title"] = "Tambah Pertemuan"
    context["need_login"] = True
    request.POST._mutable = True
    if(request.POST): form = PertemuanForm(request.POST)
    else: form = PertemuanForm(initial={'waktu_mulai': get_hour(1), 'nama':get_random_title()})

    if(not request.user.is_authenticated):
        messages.warning(request, "Login dulu untuk bisa menambahkan pertemuan!")
        if(request.method == "POST"): return redirect('kenalan:tambah_pertemuan')
    else:
        form.data["pembuat"] = request.user.id
        form.data["peserta"] = request.user.id
        
    if(form.is_valid() and request.method == "POST"):
        nama = form.cleaned_data['nama']
        messages.success(request, f'Berhasil menambahkan pertemuan {nama}')
        form.save()
        return redirect('kenalan:daftar_pertemuan')

    context["form"] = form
    return render(request, "tambah_pertemuan.html", context)

    
def hapus_pertemuan(request, id):
    if(request.method != "POST"):
        return redirect('kenalan:daftar_pertemuan')
    
    context = copy.copy(original)
    pertemuan = get_object_or_404(Pertemuan, id=id)

    if(str(pertemuan.pembuat.id) != str(request.POST["pembuat"])):
        raise Http404("Non-maker attempt to delete meeting")

    if request.method == 'POST':
        messages.success(request, f'{pertemuan} berhasil dihapus')
        pertemuan.delete()
    
    return redirect('kenalan:daftar_pertemuan')

def detail_pertemuan(request, id):
    context = copy.copy(original)
    all_pertemuan = Pertemuan.objects.all()
    for i in range(len(all_pertemuan)):
        if(all_pertemuan[i].id == id):
            context["pertemuan"] = all_pertemuan[i]
            context["next_pertemuan"] = all_pertemuan[i+1 if i+1 < len(all_pertemuan) else 0].id
            context["prev_pertemuan"] = all_pertemuan[i-1 if i > 0 else len(all_pertemuan)-1].id
            break

    if("pertemuan" not in context): raise Http404("Pertemuan tidak ditemukan!")

    context["title"] = f'{context["pertemuan"].nama}'
    context["description"] = f'Detail pertemuan {context["pertemuan"].nama}'
    context["all_peserta"] = context["pertemuan"].peserta.all()
    context["kapasitas"] = f'{len(context["all_peserta"])}/{context["pertemuan"].kapasitas}'
    context["komentars"] = context["pertemuan"].komentar.all()
    
    if(len(context["all_peserta"]) == 0):
        context["all_peserta"] = ["Hah? Kosong! 🐴"]

    if(request.user.is_authenticated):
        context["full_name"] = Profile.objects.get(id=request.user.id)
        context["cant_register"] = (context["full_name"] in context["all_peserta"])
    
    
    context["komentar_form"] = KomentarForm(request.POST or None)
    return render(request, "detail_pertemuan.html", context)

def toggle_hadir(request):
    if(request.method != "POST"):
        return redirect("kenalan:daftar_pertemuan")
    data = request.POST
    pertemuan = get_object_or_404(Pertemuan, id=data['pertemuan'])
    peserta = get_object_or_404(Profile, id=data['peserta'])
    if(peserta in pertemuan.peserta.all()):
        pertemuan.peserta.remove(peserta)
        messages.info(request, f'Kamu tidak jadi hadir di pertemuan ini!') 
    else:
        if(len(pertemuan.peserta.all()) >= pertemuan.kapasitas):
            messages.error(request, f'Belum bisa menambahkan karena kapasitasnya penuh 😔')
        else:
            pertemuan.peserta.add(peserta)
            messages.success(request, f'Kamu akan hadir di pertemuan ini!') 
    return redirect("kenalan:detail_pertemuan", id=data['pertemuan'])

def tambah_komentar(request, id):
    if not request.user.is_authenticated:
        return redirect('sso_ui:login')
    if(request.method == "POST"):
        request.POST._mutable = True
        komentarForm = KomentarForm(request.POST)
        komentarForm.data["penulis"] = request.user.id
        komentarForm.data["pertemuan"] = id
        if(komentarForm.is_valid()):
            messages.success(request, "Komentar berhasil ditambahkan")
            komentarForm.save()
    return redirect("kenalan:detail_pertemuan", id=id)