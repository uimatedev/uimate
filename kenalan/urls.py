"""SSO UI urls module."""
from django.urls import path

from . import views

app_name = 'kenalan'
urlpatterns = [
    path('', views.index, name='index'),
    path('pertemuan/', views.daftar_pertemuan, name='daftar_pertemuan'),
    path('tambah/', views.tambah_pertemuan, name='tambah_pertemuan'),
    path('toggle_hadir/', views.toggle_hadir, name='toggle_hadir'),
    path('hapus/<int:id>/', views.hapus_pertemuan, name='hapus_pertemuan'),
    path('detail/<int:id>/', views.detail_pertemuan, name='detail_pertemuan'),
    path('komentar/<int:id>/', views.tambah_komentar, name='tambah_komentar'),
]