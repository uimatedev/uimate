# Generated by Django 3.1.2 on 2020-10-29 19:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kenalan', '0013_komentar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='komentar',
            name='pertemuan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='komentar', to='kenalan.pertemuan'),
        ),
        migrations.AlterField(
            model_name='komentar',
            name='waktu',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
