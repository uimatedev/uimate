# Generated by Django 3.1.2 on 2020-10-29 06:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kenalan', '0008_auto_20201029_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pertemuan',
            name='waktu_mulai',
            field=models.DateTimeField(),
        ),
    ]
