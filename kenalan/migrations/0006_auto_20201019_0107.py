# Generated by Django 3.1.2 on 2020-10-18 18:07

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kenalan', '0005_auto_20201018_2322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pertemuan',
            options={'ordering': ['waktu_mulai']},
        ),
        migrations.AlterField(
            model_name='pertemuan',
            name='waktu_mulai',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 19, 2, 0)),
        ),
    ]
