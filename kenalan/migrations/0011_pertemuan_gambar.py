# Generated by Django 3.1.2 on 2020-10-29 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kenalan', '0010_delete_kegiatan'),
    ]

    operations = [
        migrations.AddField(
            model_name='pertemuan',
            name='gambar',
            field=models.URLField(default='https://awsimages.detik.net.id/community/media/visual/2019/02/12/43b5e043-b813-47c9-b0c1-3c8fa14bf6ae_169.jpeg'),
        ),
    ]
