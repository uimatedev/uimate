$(document).ready(function() {
    const imagePreview = document.getElementById("img-preview")
    const imageField = document.getElementById("id_gambar")
    const previewButton = document.getElementById("preview-btn")
    const imageWarning = document.getElementById("img-warning")
    previewButton.addEventListener("click", ()=>{
        imagePreview.src = "https://awsimages.detik.net.id/community/media/visual/2019/02/12/43b5e043-b813-47c9-b0c1-3c8fa14bf6ae_169.jpeg?w=700&q=90"
        imagePreview.src = imageField.value
        imageWarning.style.display = "none"
    })
    imagePreview.onerror = function(e) {
        imagePreview.src = "https://media1.tenor.com/images/6a842a184684948163e57d3920784cf2/tenor.gif?itemid=17388148"
        imageWarning.style.display = "block"
    };
});