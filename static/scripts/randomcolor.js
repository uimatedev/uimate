// from https://www.w3schools.com/howto/howto_css_smooth_scroll.asp
$(document).ready(function(){
  // Add smooth scrolling to all links
  colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'violet'];
  randomcolor = colors[Math.floor(Math.random() * colors.length)]
  contents = document.getElementsByClassName("random-color")
  for(let i = 0;i < contents.length;i++){
    contents[i].style.color = `var(--main-dark-${randomcolor})`;
  }
});
